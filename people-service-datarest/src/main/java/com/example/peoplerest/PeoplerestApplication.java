package com.example.peoplerest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PeoplerestApplication {

    public static void main(String[] args) {
        SpringApplication.run(PeoplerestApplication.class, args);
    }

}
