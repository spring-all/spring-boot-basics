# Spring Boot Basics

Example projects to show main Spring Boot modules and functionalities. Most of the explanations about Spring Boot are registered in the projects comments.

## Example Projects


### people-service-basics

A very simple Spring Boot REST service. The service provides a single *non-persistent* endpoint (/people) so one can 'crud' a person register. 

The project shows the main Spring Boot annotations and also provides a Swagger UI to test the endpoint.

### people-service-datarest

A Spring Data REST based service. The service connects to PostgreSQL database and exposes a HAL Browser to explore the /people endpoint mapped automatically from the JPA respository.

## Environment Tools

The following tools were used to build the example projects.

### IntelliJ

[Download](https://www.jetbrains.com/idea/download/) and install IntelliJ for your OS.

For linux, just download, extract the tar.gz package and run `ideia.sh`.

**NOTE:** Spring Boot Plugin is available only for Ultimate version.

### Docker CE

Follow docker installation instructions for your OS:

Windows: https://hub.docker.com/editions/community/docker-ce-desktop-windows

Ubuntu: https://docs.docker.com/install/linux/docker-ce/ubuntu/

**Important:** Don't forget to install [Docker Compose](https://docs.docker.com/compose/install/)!

### PostgreSQL & PgAdmin

Using the [`docker-compose.yml`](docker-compose.yml):

	$ docker-compose up -d postgresql pgadmin

Both PgAdmin and PostgreSQL images will be pulled from Docker Hub if not exist and both containers will start. 

A link will be stablished between the containers. 

Access `http://localhost:5555` to se pgAdmin.

	Username: user@domain.com
	Password: pguser

Right click on 'Servers' -> 'Create' -> 'Server...'

Give a name to the server and set the following connections parameters:

	Host name / address: postgresql
	Port: 5432
	Maintenance Database: postgres
	Username: postgres
	Password: postgres


**NOTE:** Using the default setup defined in the docker-compose file, if the postgresql container is deleted, all databases will be lost. For more configurations options see: https://hub.docker.com/_/postgres

