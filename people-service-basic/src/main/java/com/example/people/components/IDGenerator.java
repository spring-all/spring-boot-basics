package com.example.people.components;

import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Spring managed component to generate random uuid strings.
 *
 * One can use {@link javax.annotation.PostConstruct} and {@link javax.annotation.PreDestroy} to execute code after the
 * bean construct is called and before the bean is destroyed.
 */
@Component
public class IDGenerator {

    /**
     * Generates a random uuid string
     * @return The generated string
     */
    public String generate() {
        return UUID.randomUUID().toString();
    }

}
