package com.example.people.components.beans;

import com.example.people.entities.Person;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * People repository - stores people information in memory
 */
@AllArgsConstructor
public class PeopleRepository {

    /**
     * People registry
     */
    private List<Person> people;

    /**
     * Saves a given {@link Person} to the registry
     * @param p the {@link Person} information
     */
    public void save(Person p) {
        this.people.add(p);
    }

    /**
     * Deletes a {@link Person} from the registry
     * @param p {@link Person} to be deleted
     */
    public void delete(Person p) {
        this.people.remove(p);
    }

    /**
     * Find all registered people
     * @return A {@link List}<{@link Person}>
     */
    public List<Person> findAll() {
        return this.people;
    }

    /**
     * Find a {@link Person} by identifier
     * @param id The {@link Person} identifie
     * @return If found, the {@link Person}. Returns <code>null</code> otherwise.
     */
    public Person findById(String id) {
        return this.people.stream().filter(p -> id.equals(p.getId())).findFirst().orElse(null);
    }

    /**
     * Deletes a {@link Person} by id
     * @param id The {@link Person} identifier
     */
    public void deleteById(String id) {
        this.people.removeIf(p -> p.getId().equals(id));
    }
}
