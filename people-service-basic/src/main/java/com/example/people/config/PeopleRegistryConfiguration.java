package com.example.people.config;

import com.example.people.components.beans.PeopleRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;

/**
 * Configuration class - usually declare other beans and/or provides configuration methods
 */
@Configuration
public class PeopleRegistryConfiguration {

    /**
     * Instantiate the {@link PeopleRepository} bean, providing a new empty {@link ArrayList} as a constructor parameter
     * @return
     */
    @Bean
    public PeopleRepository peopleRegistry() {
        return new PeopleRepository(new ArrayList<>());
    }

}
