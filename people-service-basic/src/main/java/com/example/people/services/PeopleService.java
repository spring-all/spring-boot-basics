package com.example.people.services;

import com.example.people.components.IDGenerator;
import com.example.people.components.beans.PeopleRepository;
import com.example.people.entities.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service level component
 *
 * Useful to decouple the model layer from the presentation layer.
 * Can be used by multiple presentation layers.
 */
@Service
public class PeopleService {

    /**
     * {@link Autowired} is used to inject Spring managed dependencies to the component
     */
    @Autowired
    private PeopleRepository peopleRepository;

    /**
     * {@link Autowired} is used to inject Spring managed dependencies to the component
     */
    @Autowired
    IDGenerator idGenerator;

    /**
     * Creates a new {@link Person} registration and sets a new ID
     * @param p The {@link Person} description
     */
    public void create(Person p) {
        p.setId(idGenerator.generate());
        this.peopleRepository.save(p);
    }

    /**
     * Get a {@link Person} by ID
     * @param id The {@link Person} identifier
     * @return The {@link Person} description
     */
    public Person get(String id) {
        return this.peopleRepository.findById(id);
    }

    /**
     * Updates a {@link Person} description
     * @param p The {@link Person} description with updates
     */
    public void update(Person p) {
        this.peopleRepository.deleteById(p.getId());
        this.peopleRepository.save(p);
    }

    /**
     * Removes a {@link Person} description
     * @param p The {@link Person} to be removed
     */
    public void remove(Person p) {
        this.peopleRepository.delete(p);
    }

    /**
     * Removes a {@link Person} by ID.
     * @param id The {@link Person} identifier
     */
    public void remove(String id) {
        this.peopleRepository.deleteById(id);
    }

    /**
     * Retrieves all {@link Person} registries
     * @return A {@link List}<{@link Person}>
     */
    public List<Person> list() {
        return this.peopleRepository.findAll();
    }
}
