package com.example.people.entities;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Person information
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class Person {

    /**
     * Person identifier - hash string is useful in distributed systems
     */
    private String id;

    /**
     * Person full name
     */
    private String name;

    /**
     * Person contact email - person identifier
     */
    private String email;

    /**
     * Organization to which the person is associated
     */
    private String organization;

}
