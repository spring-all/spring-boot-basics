package com.example.people.controllers;

import com.example.people.controllers.exceptions.BadRequestException;
import com.example.people.controllers.exceptions.NotFoundException;
import com.example.people.entities.Person;
import com.example.people.services.PeopleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

/**
 * Presentation level component.
 *
 * The methods of a controller can be mapped to REST endpoints, so they will be called when that endpoint is reached.
 *
 * {@link RestController} - shortcut annotation that combines {@link org.springframework.stereotype.Controller} and
 * {@link ResponseBody}. The first one will declare the class as a controller stereotype. The second will configure
 * Spring to manage the output of the method return (default to json serialization).
 *
 * {@link RequestMapping} - allows to map the entire controller to a given endpoint. Can be used on methods.
 */
@RestController //@Controller + @ResponseBody
@RequestMapping("/people")
@Slf4j //Lombok log configuration
public class PeopleController {

    /**
     * {@link Autowired} is used to inject Spring managed dependencies to the component
     */
    @Autowired
    public PeopleService peopleService;

    /**
     * Lists all registered people. Mapped to the root (/people) GET endpoint.
     * @return A {@link List}<{@link Person}>
     */
    @GetMapping
    public List<Person> list() {
        return this.peopleService.list();
    }

    /**
     * Find a {@link Person} by identifier.
     *
     * Mapped to /people/{id} GET endpoint
     *
     * @param id The person identifier.
     * @return The {@link Person} if found
     */
    @GetMapping(value = "{id}")
    public Person get(@PathVariable(name="id") String id) throws NotFoundException {

        Person person = this.peopleService.get(id);

        if(person == null){
            throw new NotFoundException("Person not found!");
        }

        return person;

    }

    /**
     * Allows to register a new {@link Person} entity by POST request to /people.
     * @param p The {@link Person} description. Default: application/json
     * @return A message indicating the person was registered
     */
    @PostMapping
    public String create(@RequestBody  Person p) {

        log.info("Registering person: " + p.getName() + " - " + p.getEmail() +  " - " + p.getOrganization());

        this.peopleService.create(p);

        return "Person registered!";

    }

    /**
     * Allows to update a {@link Person} by PUT request to /people/{id} endpoint
     * @param p The {@link Person} description. Default: application/json
     * @return A message indicating the person was updated
     *
     */
    @PutMapping(value = "{id}")
    public String update(@RequestBody Person p) throws BadRequestException, NotFoundException {

        if(p.getId() == null) {
            throw new BadRequestException("The person description must have an ID to be updated");
        }

        Person existingPerson = this.peopleService.get(p.getId());

        if(existingPerson == null){
            throw new NotFoundException("Person not found to be updated!");
        }

        this.peopleService.update(p);

        return "Person register updated!";
    }

    /**
     * Deletes a person from the registry by id.
     * @param id The {@link Person} identifier
     * @return A message indicating the person was deleted
     */
    @DeleteMapping(value = "{id}")
    public String remove(@PathVariable(name = "id") String id) {
        this.peopleService.remove(id);

        return "Person removed!";
    }


}
